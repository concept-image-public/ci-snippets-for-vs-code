# CI snippets for VS Code README

This is Concept Image agency snippets collection. It is a toolbox for faster integration of WordPress / ACF template, blade, javascript and more. We use these snippets for our own projects but anyone can use it if our workflow fits for your work !
Concept Image is a digital agency based in Rennes, France. Our team of developers mostly build custom WordPress websites and Laravel applications. 

By [Concept Image](https://www.concept-image.fr/)

## Features

### Simple HTML

| Snippet | Description |
| ----------- | ----------- |
| `container` | A section markup and a div with `container` class to start a block |

### Wordpress HTML

The WordPress snippets are prefixed with `wp:`

Common options :

* `-if` : the block is wrapped in an if/endif block
* `-!` : unescaped the html output (output is escaped by default)
* `-a` : Echo from from $args variable instead of acf field (for sub template usage)
* `-o` : Add `option` to the ACF `get_field()` or `the_field()` function
* `-s` : Add a placeholder with select for common variations
* `-p` : Add a placeholder to prompt custom variation
* `-wrap` : For loops, will add a div wrapper around the loop items
* `-key` : For foreach loops, add the $key variable

| Snippet | Description | Available options |
| ----------- | ----------- | ----------- |
| `wp:title` | Echo H2 title from ACF [Text](https://www.advancedcustomfields.com/resources/text/) field wrapped in h2 markup | -if -! |
| `wp:hn` | Echo Hn (select) title from ACF [Text](https://www.advancedcustomfields.com/resources/text/) field wrapped in hn markup | -if -! |
| `wp:desc` | Echo unescaped raw html content from ACF [Wysiwyg Editor](https://www.advancedcustomfields.com/resources/wysiwyg-editor/) field wrapped in div markup with wysiwyg class | -if -! -a |
| `wp:text` | Echo text value from ACF [Text](https://www.advancedcustomfields.com/resources/text/) field wrapped in paragraph markup | -if -! |
| `wp:field` | Echo generic text value from ACF field wrapped in div, span or paragraph markup. Prompt the name of the field | -if -! -o -a |
| `wp:image` | Load image ID from ACF [Image](https://www.advancedcustomfields.com/resources/image/) field and echo responsive image with build-in [wp_get_attachment_image()](https://developer.wordpress.org/reference/functions/wp_get_attachment_image/) | -s -p |
| `wp:link` | Echo a link from ACF [Link](https://www.advancedcustomfields.com/resources/link/) field | -a |
| `wp:gravity` | Echo a Gravity Form from an  ACF [Select](https://www.advancedcustomfields.com/resources/select/) field with function [gravity_form()](https://docs.gravityforms.com/adding-a-form-to-the-theme-file/) | |
| `wp:items` | Echo a simple loop from an ACF [Repeater](https://www.advancedcustomfields.com/resources/repeater/) field | -div -a |
| `wp:date` | Echo a date from $post date or ACF [Date](https://www.advancedcustomfields.com/resources/date-picker/) field wrapped in `time` markup | -acf -i18n |
| `wp:picture` | Echo an image from ACF [Image](https://www.advancedcustomfields.com/resources/image/) field in a picture markup with responsive source |
| `wp:slider` | Echo a basic [Swiper](https://swiperjs.com/swiper-api#swiper-full-html-layout) markup | |
| `wp:repeater` | Echo a foreach loop from an ACF [Repeater](https://www.advancedcustomfields.com/resources/repeater/) field | -wrap -key |
| `wp:query` | A [WP query](https://developer.wordpress.org/reference/classes/wp_query/#custom-field-post-meta-parameters) and a foreach loop with a get template part to echo output | See WP Query options |

WP Query options in HTML scope:
* `-pagination`: Use pagination and echo pagination with CI custom_pagination() function
* `-tax`: Add tax query parameters
* `-meta`: Add meta key value parameters
* `-num`: Add meta key value num parameters
* `-meta_query`: Add meta query parameters
* `-compare`: Add meta compare operator
* `-multiple`: Add relation for multiple tax query or meta query parameter
* `-search`: Add search query parameter
* `-query_var`: Get term from get_queried_object(), useful for taxonomy archive page
* `-if`: Add tax query or meta query if $_GET parameter exists

### Wordpress PHP

The WordPress snippets are prefixed with `wp:`

| Snippet | Description | Available options |
| ----------- | ----------- | ----------- |
| `wp:query` | Base code for WP_Query(). Use `wp:query_args` to add more parameters | -pagination |
| `wp:query_args` | Add parameters in $args array for WP_Query() | See WP Query options |

WP Query options in PHP scope:
* `-tax`: Add tax query parameters
* `-array`: Add tax query or meta query array parameters to put INSIDE tax_query or meta_query array
* `-operator`: With tax_query operator parameter
* `-tax_query`: Add [tax query](https://developer.wordpress.org/reference/classes/wp_query/#taxonomy-parameters) array
* `-meta`: Add meta query parameters
* `-meta_query`: Add [meta query](https://developer.wordpress.org/reference/classes/wp_query/#custom-field-post-meta-parameters) array
* `-fields`: Add [return fields](https://developer.wordpress.org/reference/classes/wp_query/#return-fields-parameter) parameter
* `-status`: Add [status](https://developer.wordpress.org/reference/classes/wp_query/#status-parameters) parameter
* `-p -name -page_id -pagename -post_parent -post_parent__in -post_parent__not_in -post__in -post__not_in -post_name__in`: Add [Post & Page Parameters](https://developer.wordpress.org/reference/classes/wp_query/#post-page-parameters)
* `-meta_key`: The meta key parameter
* `-meta_value`: The meta value parameter
* `-meta_value_num`: The meta value num parameter
* `-meta_compare`: The meta compare operator parameter
* `-if`: Add tax query or meta query if $_GET parameter exists

More to come...

## Example

Create an empty my-bloc.php in your WordPress theme folder

Use `container` snippet to set up basic markup

Then `wp:title`, `wp:desc`, `wp:link` and `wp:image`

![screenshot](images/screenshot.gif)

You have now a simple text and image bloc. Now you can register your ACF field and start writing your CSS style !

## Requirements

The HTML snippets are for PHP files outside of the PHP tags.
The PHP snippets are for PHP files inside of the PHP tags.
The WordPress snippets require ACF PRO and use WordPress and ACF PHP functions.

## Release Notes

### 0.0.4

Add wp:query in PHP scope