# Change Log

All notable changes to the "ci-snippets-for-vs-code" extension will be documented in this file.

## [Unreleased]

## [0.0.1] - 2023-10-18

- Initial release

## [0.0.2] - 2023-10-18

- Add `wp:repeater`

## [0.0.3] - 2023-11-07

- Add `wp:query`

## [0.0.4] - 2023-11-15

- Add `wp:query` in PHP scope
- Replace `!empty($query->posts)` by `$query->post_count`